# Summary #

Simply outputs the URL for the latest question by querying the Atlassian Answers API.

Completed as a part of a code test by Dennis at Atlassian SF.